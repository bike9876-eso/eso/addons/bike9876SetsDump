# addons/bike9876SetsDump

## Dump non-craftable ESO sets to disk and display results

### Preliminary Notes

In the below, `ESOLIVEDIR` is defined as follows:

On Windows,

      ESOLIVEDIR="%UserProfile%\Documents\Elder Scrolls Online\live"

(%UserProfile% is usually `C:\Users\YOUR-WINDOWS-USERNAME`).

On Windows using the [linux subsystem on windows](https://learn.microsoft.com/en-us/windows/wsl/install) (WSL),

      ESOLIVEDIR="/mnt/c/Users/YOUR-WINDOWS-USERNAME/Documents/Elder Scrolls Online/live"

On debian linux running steam/proton,

      ESOLIVEDIR="$HOME/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My Documents/Elder Scrolls Online/live"

On Arch Linux,

      ESOLIVEDIR="$HOME/.local/share/Steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My Documents/Elder Scrolls Online/live"

It will be something similar for other linux distributions.

ESO game settings, addons, addon output files are all saved under `ESOLIVEDIR`.

Windows and linux use a different separator between path components ('\\' on Windows, '/' on linux). I'll use '/'
only below for simplicity.

### Introduction

It's often useful to be able to see what pieces in non-craftable sets have been unlocked. ESO can of course do this
(Collections -> Sets Items). But I often want to see this information when not logged into ESO, or I want all the
data in a text file, so I can search through it using an editor.

This addon dumps all the non-craftable set info to disk whenever the user logs out, quits or runs /reloadui.

An accompanying script turns this dumped data into a plain text table showing what pieces the user has learned.

It also lists the number of known pieces for each of the Undaunted Pledge Master coffers (can be helpful eg in deciding who to buy a Mystery Coffer from).

### Prerequisites

For the addon, the following addons are prerequisites:

- LibSets
- LibZone

These are most easily installed with [minion](https://minion.mmoui.com/).

For the helper script I provide `make_set_table.py` (see below for details), the following are prerequisites:

- python3

- python3 packages: tabulate, lupa, wcwidth

(wcwidth is only needed if your language is Japanese or Chinese, but there's no harm in installing it).

These would normally be provided by a linux distribution, eg (for debian or ubuntu) in `python3-PACKAGE`, so run

      sudo apt install python3-tabulate python3-lupa python3-wcwidth

Or install with pip3: `pip3 install PACKAGE` for all the packages.

If running windows, suggest install the linux subsystem on windows (WSL).
If you install the debian or ubuntu distributions, python3 will be installed, and you can use the above `sudo apt install` command
to install the needed python3 packages.

### Download and Install

Download [https://gitlab.com/bike9876-eso/eso/addons/bike9876SetsDump/-/archive/v1.3.3/bike9876SetsDump-v1.3.3.zip](https://gitlab.com/bike9876-eso/eso/addons/bike9876SetsDump/-/archive/v1.3.3/bike9876SetsDump-v1.3.3.zip).

Unpack the zip file.

The extracted files will look like [^1]:

        bike9876SetsDump-v1.3.3/
        ├── Addons/
        │   └── bike9876SetsDump/
        │       ├── bike9876SetsDump.lua
        │       ├── ...
        │
        ├── LICENSE
        ├── make_set_table.py
        ├── ...

Directory `bike9876SetsDump` (under `Addons`) is an addon.

Copy or move it to the ESO addons directory `ESOLIVEDIR/AddOns/`.

So afterwards, there should be a directory `bike9876SetsDump` under the ESO addons directory.

If you install/update the addon while ESO is running, you will need to logout and login (or run /reloadui) for it to be available within the game.

[^1]: Some zip extractors will create a directory above `bike9876SetsDump-v1.3.3`, also called `bike9876SetsDump-v1.3.3`.

### Usage

When you logout or quit ESO (or run /reloadui), this addon will automatically dump details about your non-craftable sets to `bike9876SetsDump.lua`
under your ESO SavedVariables directory `ESOLIVEDIR/SavedVariables/`.

To make a simple plain text display of your sets:

Change directory to `bike9876SetsDump-v1.3.3` (the directory created by extracting `bike9876SetsDump-v1.3.3.zip` in [Download and Install](#download-and-install)).

Copy `bike9876SetsDump.lua` from your ESO SavedVariables directory to here.

Run

      python3 make_set_table.py --server EU > setsDump.txt

or

      python3 make_set_table.py --server NA > setsDump.txt

(depending on whether you use the EU or NA server [^2]).

This assumes you are running linux (possibly the linux subsystem on Windows). If you have python running directly on Windows, the command needed
will probably be similar.

Open `setsDump.txt` in an editor. It contains a set of tables, one for each non-craftable set, showing which pieces you have learned
in each set (a `+` indicates you have learned it).

After this set of tables, it then lists the Undaunted Pledge Masters, and how many pieces you know from each of their coffers.
(The names of the Pledge Masters and of the Coffers is currently only given in English.)

[^2]: If you only use one server, you don't need the --server \<server\> argument (see [Notes](#notes)), but there's no harm in specifying it.

### Notes

Knowledge of non-craftable sets is shared across all characters (in a given server), so only one character needs to have the addon installed.

The `make_set_table.py` script has some options. Its usage is:

      python3 make_set_table.py [--help] [--dumpfile DUMPFILE] [--server SERVER] [--account ACCOUNT] [--setname-maxcolwidth SETNAME-MAXCOLWIDTH] [--ncols NCOLS]

The options are:

      --help

lists the available options for the `make_set_table.py` script.

      --dumpfile DUMPFILE

specifies that the file to be read in containing dumped set data is DUMPFILE (instead of the default `bike9876SetsDump.lua` in the current directory).

      --server SERVER

is needed if you have account on more than 1 server (eg both the EU and NA servers), and if the addon is used for these servers. Then specify eg
`--server EU` to display the data for accounts on only the EU server. Specify `--server NA` for the NA server.

      --account ACCOUNTNAME
is needed if you have more than 1 account on the server (unlikely, but possible). Eg `--account @bike9876` .

      --setname-maxcolwidth SETNAME-MAXCOLWIDTH

changes the max width of the column displaying the set name in `make_set_table.py`'s output (default is 9 characters). Setting it to a large
value, eg `--setname-maxcolwidth 100` ensures that the set names are not split across lines, which is possibly useful when searching for a name.

      --ncols NCOLS

sets the max number of columns used in the table for each set. Default is 9. Setting it to a large value, eg `--ncols 100` ensures that the data
for each set is on a single row (but some rows will be very long).

### License

This software is released under the MIT License. Please see LICENSE.

<a href="https://エボ猫.コム/">bike9876@エボ猫.コム</a>
