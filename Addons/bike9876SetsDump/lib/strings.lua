-- strings.lua
-- @bike9876
-- Nov 2022
-- See LICENSE
-- utility functions for strings

if evocatnet == nil then
    evocatnet = {}
end

if evocatnet.lib == nil then
    evocatnet.lib = {}
end

evocatnet.lib.strings = {}

-- escape any "|"s in a string (so can send to eso chat window and pipes will appear as pipes)
local function pipe_escape(str)
    local str2 = str:gsub('|','||')
    return str2
end
evocatnet.lib.strings.pipe_escape = pipe_escape

-- split a string <str> according to the given pattern <del> (eg '%s+')
-- If <del> does not exist, {<str>} is returned.
local function split(str,del)
    local strs = {}
    local pos = 1
    while true do
        local start,stop = str:find(del,pos)
        -- print('start = ',start,', stop = ',stop)
        if start then
            table.insert(strs,str:sub(pos,start-1))
        else
            table.insert(strs,str:sub(pos))
            return strs
        end
        pos = stop+1
    end
end
evocatnet.lib.strings.split = split

-- split a string into substrings of length <n> (the last substring may have length < <n>)
local function split_n(str,n)
    local strs = {}
    for j=1,math.ceil(string.len(str)/n) do
        table.insert(strs,string.sub(str,(j-1)*n+1,j*n))
    end
    return strs
end
evocatnet.lib.strings.split_n = split_n

return evocatnet.lib.strings
