-- tables.lua
-- @bike9876
-- July 2022
-- See LICENSE
-- utility functions for tables/lists

if evocatnet == nil then
    evocatnet = {}
end

if evocatnet.lib == nil then
    evocatnet.lib = {}
end

evocatnet.lib.tables = {}

-- returns true if table t is empty (it is assumed to be a table)
local function isempty(t)
    local n,tab,k = pairs(t)
    local k2,v2 = n(tab,k)
    return k2 == nil
end
evocatnet.lib.tables.isempty = isempty

-- returns true if l is a list (it is assumed to be a table)
-- a list is taken to be a table with all keys integers >= 1
-- (includes if is an empty list)
-- A list may have holes, eg {'a','b',nil,'c'} or l={}; l[1]=32; l[100]=64
local function islist(l)
    for k,v in pairs(l) do
        if type(k) ~= 'number' then
            return false
        end
        if k<1 then
            return false
        end
        if not tostring(k):match('^%d+$') then
            return false
        end
    end
    return true
end
evocatnet.lib.tables.islist = islist

-- returns the number of elements in a sequence s (a sequence is a list {'x','y',...} with no holes (eg {'x','y',nil,'z'} has a hole)
-- (s is assumed to be a table)
-- (see https://www.lua.org/manual/5.4/manual.html#3.4.7 re sequences)
-- returns false if s is not a sequence.
-- (returns 0 if s is the empty sequence)
local function nsequence(s)
    local n = 1
    for k,v in pairs(s) do
        if k ~= n then
            return false
        end
        n = n + 1
    end
    return n-1
end
evocatnet.lib.tables.nsequence = nsequence

-- return a list of the keys in table t
local function keys(t)
    local keys = {}
    local n = 1
    for k,_ in pairs(t) do
        table.insert(keys,k)
    end
    return keys
end
evocatnet.lib.tables.keys = keys

-- return the "inverse" of a table, eg {a='x', b='y'} -> {x='a', y='b'}
local function invert(t)
    local t2 = {}
    for k,v in pairs(t) do
        t2[v] = k
    end
    return t2
end
evocatnet.lib.tables.invert = invert

-- return the number of elements in table t
local function ntab(t)
    local nt = 0
    for j,v in pairs(t) do
        nt = nt+1
    end
    return nt
end
evocatnet.lib.tables.ntab = ntab

-- return true if k is an identifier (that is not a reserved word)
local function is_identifier(k)
    local reserved = { -- as of lua 5.4
        ["and"]=true,       ["break"]=true,     ["do"]=true,        ["else"]=true,      ["elseif"]=true,
        ["end"]=true,       ["false"]=true,     ["for"]=true,       ["function"]=true,  ["goto"]=true,
        ["if"]=true,        ["in"]=true,        ["local"]=true,     ["nil"]=true,       ["not"]=true,
        ["or"]=true,        ["repeat"]=true,    ["return"]=true,    ["then"]=true,      ["true"]=true,
        ["until"]=true,     ["while"]=true,
    }

    if type(k) ~= 'string' then
        return false
    end
    if reserved[k] then
        return false
    end
    if k:match('^[a-zA-Z_][a-zA-Z0-9_]*$') then
        return true
    end
    return false
end
evocatnet.lib.tables.is_identifier = is_identifier

-- return a variable as a string representation.
-- opts = {seq_print=true, short_form=true, max_depth=10, ...}
-- seq_print=true: print a sequence using {x,y,z,...} notation. Default false.
-- short_form=true: print a table using short form {x=2, y=3, ...} instead of {["x"]=2, ["y"]=3, ...} where possible. Default false.
-- max_depth=<max_depth>: descend a max depth of <max_depth> into a nested table (depth=1 is top-level table). Default nil. nil means no max depth.
--   An element that would be deeper than <max_depth> is output as *.
--
-- opts apply to both the table t, and any keys in any tables that happen to be tables.
local function as_str(t,opts,depth)
    depth = depth or 1
    if type(t) ~= 'table' then
        if type(t) == 'string' then
            return string.format('%q',t)
        else
            return tostring(t)
        end
    end

    local is_sequence,short_form,max_depth
    local str = "{"
    local first = true
    local kstr,vstr
    if opts ~= nil then
        if opts.seq_print and nsequence(t) then
            is_sequence = true
        end
        if opts.short_form then
            short_form = true
        end
        max_depth = opts.max_depth
    end
    for k,v in pairs(t) do
        if not first then
            str = str .. ','
        end
        if max_depth and depth >= max_depth then
            vstr = '*'
        else
            vstr = as_str(v,opts,depth+1)
        end
        if is_sequence then
            str = str .. vstr
        else
            if short_form and is_identifier(k) then
                kstr = k
            else
                kstr = '['..as_str(k,opts)..']'
            end
            str = str .. kstr .. '=' .. vstr
        end
        first = false
    end
    str = str .. "}"
    return str
end
evocatnet.lib.tables.as_str = as_str

return evocatnet.lib.tables
