--[[
bike9876SetsDump

bike9876@エボ猫.コム
See LICENSE

Dump a lua table listing the non-craftable sets known (ie in the stickerbook).
(This is account-wide - ie if character A deconstructs a piece in a set, all characters
in that account will learn the piece.)
Typically this would then be processed by some offline program to make eg a nice table.

On /reloadui, or logging out or quitting, setsDump is filled with the sets data,
which is then saved to disk.
The dump will be in the eso addons SavedVariables/ directory, filename bike9876SetsDump.lua.

]]

local debug
-- debug = true -- comment out to disable

local startlog = {}

local thisAddonName = 'bike9876SetsDump'
local setsDump
local currentCharName = GetUnitName('player')

local libSets = LibSets

local strings = evocatnet.lib.strings
local tables = evocatnet.lib.tables

------------------------------------------------------------------------

--[[
slash commands:

set <n>: output on chat window info about the stickerbook for setid <n>.

setids : list all set ids

dump: dump set data (ie save into setsDump -- will be saved to SavedVariables file on logout etc.)
 - since this addon creates the dump automatically, this command should not be needed.

clear: clear setsDump
]]

----------------------------------------

-- Get relevant info on set with id itemSetId
-- Returns {name=<setName>,catId=<catID>,zoneIds=<zoneIds>,numPieces=<numPieces>,
--            setType=<setType>,setTypeName=<setTypeName>,
--            dlcName=<dlcName>, dlcTypeName=<dlcTypeName>,
--            armor={[<pieceid>={'Head',1,'Light',true},...},weapons={[<pieceid>]=>{'Two-Handed',12,'Flame Staff',false},...}
--         }
-- where catId is the eso catid giving where the set can be found,
-- zoneIds={fromCatId={...},fromSetId={...}} lists where the zoneids corresponding to the catid, and to the setid.
-- dlcName and dlcTypeName also specify (as strings) also specify where the set can be found (see below),
-- numPieces is how many pieces are in the set.
-- The table eg {1,'Head',1,'Light',true} is {<equipment_type>,<equipment_type_name>, <armor_type>,<armor type_name>,<is_unlocked>}
-- {6,'Two-Handed',12,'Flame Staff',false} is {<equipment_type>,<equipment_type_name>,<weapon_type>,<weapon type_name>,<is_unlocked>}
-- If the set is craftable, then numPieces=0 and armor and weapons are empty.
--
-- Note I don't use setInfo = libSets.GetSetInfo(itemSetId) to get the info as it returns an element for every piece/trait combination,
-- which is more than I want,
-- eg for itemSetId=269 (Chokethorn),
-- setInfo.setItemIds={
--   [94492]=1,[94493]=1,...,[94499]=1, -- Visage (Heavy Head), different traits
--   [94628]=1,[94629]=1,...,[94635]=1, -- Pauldrons
--   ..
--   [95036]=1,[95037]=1,...,[95039]=1, -- Mask (Medium Head) ] for some reason these are split
--   [95040]=1,[95041]=1,...,[95043]=1, -- Mask (Medium Head) ] into two separate ranges
--   [95172]=1,[95173]=1,...,[95179]=1, -- Arm Cops
--   } -- see lib.setDataPreloaded[LIBSETS_TABLEKEY_SETITEMIDS][269], where eg an entry [1]="94492,7" means 94492,94493,...,94499
--
-- zoneNames is the current set of zone names we know correponding to zoneIds. It is updated by the call to this function.

local function get_setinfo(itemSetId,zoneNames)
    local setInfo = {}

    if debug then
        d(string.format('itemSetId = %d',itemSetId))
    end

    setInfo.name = libSets.GetSetName(itemSetId)

    -- itemSetCollectionCategoryId identifies where the item is found (or 0 if item is craftable).
    -- See usage of GetItemSetCollectionCategoryId() at esoui/ingame/collections/itemsetcollectionsdata.lua::ZO_ItemSetCollectionData:Initialize().
    -- See lib.setDataPreloaded.LIBSETS_TABLEKEY_SET_ITEM_COLLECTIONS_ZONE_MAPPING[*].category in LibSets/LibSets_Data_All.lua for category values.
    -- eg
    -- itemSetId=207 ("Law of Julianos") -> itemSetCollectionCategoryId=0 (item is craftable)
    -- itemSetId=36 ("Armor of the Veiled Heritance") -> itemSetCollectionCategoryId=11 (22 piece set from Aldmeri Dominion -> Auridon)
    -- itemSetId=269 ("Chokethorn") -> itemSetCollectionCategoryId=47 (6 piece monster set from Dungeons -> Elden Hollow)
    -- itemSetId=614 ("Hexos' Ward") -> itemSetCollectionCategoryId=97 (22 piece set from DLC Zones -> Deadlands (but can drop in Deadlands and Fargrave))
    --
    -- some sets (eg setId 120 "Arms of Infernace") no longer drop in game, and have catId = 0
    local itemSetCollectionCategoryId = GetItemSetCollectionCategoryId(itemSetId)
    setInfo.catId = itemSetCollectionCategoryId

    setInfo.zoneIds = {}

    setInfo.zoneIds.fromCatId = libSets.GetItemSetCollectionZoneIds(itemSetCollectionCategoryId) -- a table getting the zoneIds corresponding to the categoryId 

    -- zoneIds from the setId
    -- Unless overridden (see the libSets_SetData.xlsx spreadsheet at https://github.com/Baertram/LibSets/raw/LibSets-reworked/LibSets/LibSets_SetData.xlsx)
    -- the first 3 values are the zoneids of the wayshrines near where the set is dropped (for AD, DC, and EP). Subsequent values are manually added.
    -- The wayshrine ids (and hence the zoneids are normally all the same). Examples when they are not:
    -- setId 501 (Thrassian Stranglers): wayshrine ids: (AD) 176, (DC) 360, (EP) 179
    -- zone ids (from ESO dumped Wayshrineinfo table in spreadsheet): 381 (Auridon), 1027 (Artaeum), 534 (Stros M'Kai)
    setInfo.zoneIds.fromSetId = libSets.GetZoneIds(itemSetId)

    -- get zoneIds both ways as eg
    -- setId=587 (Bahsei's Mania) has itemSetCollectionCategoryId=94, zoneIds.fromCatId=nil, zoneIds.fromSetId={1263,1261,1261} (={"Rockgrove","Blackwood","Blackwood"})
    -- setId=653 (Perfected Whorl of the Depths) has zoneIds.fromCatId={1344} (="Dreadsail Reef"), zoneIds.fromSetId={1318,1318,1318} (="High Isle")
    -- so sometimes zoneIds.fromCatId has no information, and sometimes zoneIds.fromSetId is too vague.

    -- add zone names for these zoneIds to the current list of known zone names
    for zoneIdSource,zoneIdsFromSource in pairs(setInfo.zoneIds) do
        if zoneIdsFromSource ~= nil then
            for _,zoneId in pairs(zoneIdsFromSource) do
                if zoneId ~= 0 then  -- get setInfo.zoneIds.fromSetId = 0 for setIds (eg) 627, 656, 657 (don't include 0 as a zoneId)
                    zoneName = libSets.GetZoneName(zoneId) -- use the LibSets wrapper to LibZone:GetZoneName, as LibSets also uses "special" zoneId values that it deals with itself
                    if zoneName ~= "" then
                        zoneNames[zoneId] = zoneName
                    else
                        d(string.format('(itemSetId=%d) no zoneName for zoneId %d',itemSetId,zoneId))
                        d(string.format('catId = %d',setInfo.catId))
                        d(string.format('zoneIds = %s',tables.as_str(setInfo.zoneIds)))
                    end
                end
            end
        end
    end

    -- get dropmechanics
    -- local dropMechanics,dropMechanicNames,dropMechanicTooltips,dropMechanicLocationNames,dropZoneIds = libSets.GetDropMechanic(itemSetId,true)
    -- setInfo.drop = {mechanics=dropMechanics, names=dropMechanicNames, tooltips=dropMechanicTooltips, locationNames=dropMechanicLocationNames, zoneIDs=dropZoneIds}

    -- see LibSets/LibSets_ConstantsLibraryInternal.lua possibleSetTypes
    -- eg
    -- setType=6, setTypeName="Dungeon"
    -- setType=9, setTypeName="Overland"
    setInfo.setType = libSets.GetSetType(itemSetId)
    setInfo.setTypeName = libSets.GetSetTypeName(setInfo.setType)

    -- eg
    -- dlcId=0 (DLC_BASE_GAME), dlcName="Elder Scrolls Online", dlcType=0 (DLC_TYPE_BASE_GAME), dlcTypeName="" (not DLC) for Armor of the Veiled Heritance set (Aldmeri Dominion -> Auridon set)
    -- dlcId=2 (DLC_ORSINIUM), dlcName="Orsinium", dlcType=1 (DLC_TYPE_CHAPTER), dlcTypeName="DLC_TYPE_CHAPTER) for Mark of the Pariah (DLC Zones -> Wrothgar)
    -- dlcId=21 (DLC_FLAMES_OF_AMBITION), dlcName="Flames of Ambition", dlcType=2, dlcTypeName="DLC_TYPE_DUNGEONS" for Drake's Rush (DLC Dungeons -> Black Drake Villa set)
    -- dlcId=24 (DLC_DEADLANDS), dlcName="The Deadlands",  dlcType=3, dlcTypeName="DLC_TYPE_ZONE" for Hexos' Ward set (DLC Zones -> The Deadlands set)
    -- (See LibSets_Constants_All.lua possibleDlcIds for dlcId values,
    -- possibleDlcTypes for dlcType values (DLC_TYPE_BASE_GAME, DLC_TYPE_CHAPTER, DLC_TYPE_DUNGEONS, DLC_TYPE_ZONE),
    -- lib.dlcAndChapterCollectibleIds for dlcType for each dlcId.)
    local dlcId = libSets.GetDLCId(itemSetId)
    setInfo.dlcName = libSets.GetDLCName(dlcId)
    local dlcType = libSets.GetDLCType(itemSetId)
    setInfo.dlcTypeName = libSets.GetDLCTypeName(dlcType)

    -- number of pieces in set (0 if a craftable set)
    -- eg
    -- itemSetId=207 -> numPieces=0
    -- itemSetId=36 -> numPieces=22
    -- itemSetId=269 -> numPieces=6
    -- itemSetId=614 -> numPieces=22
    local numPieces = GetNumItemSetCollectionPieces(itemSetId) -- see usage at esoui/ingame/collections/itemsetcollectionsdata.lua::ZO_ItemSetCollectionData:Initialize()
    setInfo.numPieces = numPieces

    setInfo.armor={}
    setInfo.weapons={}

    libSets_setInfo = libSets.GetSetInfo(itemSetId,true) -- don't need itemIds subtable
    if libSets_setInfo.undauntedChestId then
        -- eg for Spawn of Mephala (setId = 162), undauntedChestId=2, undauntedName="Glirion the Redbeard" (in english)
        setInfo.undauntedChestId = libSets_setInfo.undauntedChestId
        setInfo.undauntedName = libSets.GetUndauntedChestName(libSets_setInfo.undauntedChestId)
    end 

    for i=1,numPieces do
        local pieceId, slot = GetItemSetCollectionPieceInfo(itemSetId, i)

        local itemLink = GetItemSetCollectionPieceItemLink(pieceId,LINK_STYLE_DEFAULT,ITEM_TRAIT_TYPE_NONE)
        local isSlotUnlocked = IsItemSetCollectionSlotUnlocked(itemSetId,slot) -- unlocked if have deconstructed item

        --[[
        local equipmentFilterType = GetEquipmentFilterTypeForItemSetCollectionSlot(slot)
        -- eg equipmentFilterType=2 ("Medium Armor")
        -- see EQUIPMENT_FILTER_TYPE_BOW etc in https://wiki.esoui.com/Constant_Values for possible values:
        -- BOW (11), DESTRO_STAFF (9), HEAVY (3), LIGHT (1), MEDIUM (2), NECK (4), ONE_HANDED (6), RESTO_STAFF (10), RING (5), SHIELD (7), TWO_HANDED (8)
        d(string.format('equipmentFilterType=%d ("%s")',equipmentFilterType,GetString('SI_EQUIPMENTFILTERTYPE',equipmentFilterType)))
        ]]

        -- see esoui/publicallingames/tooltip/itemtooltips.lua::ZO_Tooltip:AddTopSection for use of GetItemLinkItemType etc
        local itemType = GetItemLinkItemType(itemLink)
        local itemTypeName,armor_or_weapon_type,armor_or_weapon_typeName
        local equipType = GetItemLinkEquipType(itemLink)
        local weaponType = GetItemLinkWeaponType(itemLink)
        if itemType == ITEMTYPE_ARMOR and weaponType == WEAPONTYPE_NONE then
            itemTypeName = "armor"
            local armorType = GetItemLinkArmorType(itemLink)
            armor_or_weapon_type = armorType
            armor_or_weapon_typeName = GetString("SI_ARMORTYPE",armorType) -- eg 'Light'
        elseif weaponType ~= WEAPONTYPE_NONE then
            itemTypeName = "weapons"
            armor_or_weapon_type = weaponType
            armor_or_weapon_typeName = GetString("SI_WEAPONTYPE",weaponType) -- eg 'Flame Staff'
        else
            d(string.format('ERROR: itemLink=%s, itemType=%s, equipType=%s, weaponType=%s (could not identify, skipping item)',itemLink,tables.as_str(itemType),tables.as_str(equipType),tables.as_str(weaponType)))
        end

        if itemTypeName then
            setInfo[itemTypeName][pieceId] = {equipType,GetString("SI_EQUIPTYPE",equipType),armor_or_weapon_type,armor_or_weapon_typeName,isSlotUnlocked} -- eg setInfo.armor[85651] = {1,'Head',1,'Light',true}, or setInfo.weapons[85643] = {6,'Two-Handed',12,'Flame Staff',false}
        end
    end

    return setInfo
end

local function do_set(itemSetId)
    local debug_bak = debug
    debug = true
    if not LibSets.checkIfSetsAreLoadedProperly() then
        d('LibSets not ready, try later')
    end
    zoneNames = {}
    d(tables.as_str(get_setinfo(itemSetId,zoneNames)))
    d(tables.as_str(zoneNames))
    debug = debug_bak
end

----------------------------------------

-- do_setids
-- list all ids corresponding to non-craftable sets

local function get_non_craftable_setIds()
    local allSetIds = libSets.GetAllSetIds()
    local non_craftable_setIds = {}
    for n,_ in pairs(allSetIds) do
        if not libSets.IsCraftedSet(n) then
            table.insert(non_craftable_setIds,n)
        end
    end
    return non_craftable_setIds
end

local function do_setids()
    d(tables.as_str(get_non_craftable_setIds(),{seq_print=true}))
end

----------------------------------------

-- do_clear
-- Clear setsDump

local function do_clear()
    setsDump.sets = {}
    setsDump.zoneNames = {}
    setsDump.constants = {}
    setsDump.i18n = {}
end

----------------------------------------

-- set_constants(t)
--
-- set constants defined by libSets, that might be useful to an external program
-- Table t is modified to save these constants.

local function set_constants(t)
    --t.dropMechanic = {}
    t.setType = {}
    for k,v in zo_insecurePairs(_G) do
        --if k:match('^LIBSETS_DROP_MECHANIC_') then
        --    t.dropMechanic[v] = k -- eg t.dropMechanic[1] = 'LIBSETS_DROP_MECHANIC_MAIL_PVP_REWARDS_FOR_THE_WORTHY'
        --end
        if k:match('^LIBSETS_SETTYPE_') then
            t.setType[v] = k -- eg t.setType[1] = 'LIBSETS_SETTYPE_ARENA'
        end
    end
end

----------------------------------------

-- do_dump
-- Save non-craftable set info to setsDump.

local function do_dump()
    if not LibSets.checkIfSetsAreLoadedProperly() then
        d('LibSets not ready, will not be able to dump sets (try relogging in and trying again)')
        return
    end

    do_clear()

    setsDump.clientLang = GetCVar("language.2")

    setsDump.i18n.SI_MASTER_WRIT_DESCRIPTION_SET = GetString(SI_MASTER_WRIT_DESCRIPTION_SET) -- eg "Set", "Conjunto", "Ensemble"

    set_constants(setsDump.constants)

    for _,n in ipairs(get_non_craftable_setIds()) do
        setsDump.sets[n] = get_setinfo(n,setsDump.zoneNames)
    end

    d(string.format('%s: sets saved',thisAddonName))
end

----------------------------------------

local function slash(cmd)

    d(cmd)

    local args = strings.split(cmd,'%s+')

    local cmd_type = args[1]

    if cmd_type == nil then
        d(string.format('bad cmd "%s"',cmd))
        return
    end

    if cmd_type == 'set' then -- set <n> (show what sets we have with setid <n>)
        if not args[2] then
            d(string.format('bad cmd "%s"',cmd))
            return
        end
        local n = tonumber(args[2])
        if n == nil then
            d(string.format('set <n>: <n> not a number ("%s")',args[2]))
            return
        end
        do_set(n)
    elseif cmd_type == 'setids' then -- list all set ids
        do_setids()
    elseif cmd_type == 'dump' then -- dump sets data
        do_dump()
    elseif cmd_type == 'clear' then -- clear sets data dump
        do_clear()
    else
        d(string.format('bad cmd "%s"',cmd))
        return
    end

end

------------------------------------------------------------------------
--
-- output startlog (chat window should be available now) -- need to
-- put inside zo_callLater, else does not appear when login.

local function onPlayerActivated(eventID,initial)
    zo_callLater(function()
        for _,msg in ipairs(startlog) do
            d(msg)
        end
        startlog = {}
    end, 10)
end

------------------------------------------------------------------------

-- set up hooks to catch setcvar/reloadui/logout/quit (all of which cause SavedVariables to be saved)

local function setupHooks()
    ZO_PreHook("ReloadUI", function()
        do_dump()
    end)

    ZO_PreHook("SetCVar", function()
        do_dump()
    end)

    ZO_PreHook("Logout", function()
        do_dump()
    end)

    ZO_PreHook("Quit", function()
        do_dump()
    end)
end

------------------------------------------------------------------------

local function initialize()
    if debug then
        table.insert(startlog, string.format('%s: initialize',thisAddonName))
    end
    -- save an account-wide SavedVariables file, listing all the known non-craftable sets.
    setsDump = ZO_SavedVars:NewAccountWide("bike9876SetsDumpVar",1,nil,{lang='',sets={},zoneNames={},constants={},i18n={}},GetWorldName())

    setupHooks()
    EVENT_MANAGER:RegisterForEvent(thisAddonName .. "PlayerActivated", EVENT_PLAYER_ACTIVATED, onPlayerActivated)
end

------------------------------------------------------------------------

local function onAddOnLoaded(eventID, addonName)
  if addonName == thisAddonName then
      SLASH_COMMANDS['/bike9876setsdump'] = slash -- add "/bike9876setsdump" command (note all slash commands must be all lower-case)
      initialize()
  end
end

EVENT_MANAGER:RegisterForEvent(thisAddonName .. "AddOnLoaded", EVENT_ADD_ON_LOADED, onAddOnLoaded)
