# Functions handling SavedVariables files
#
# @bike9876 2024

import sys
import re
import pprint

from lupa import LuaRuntime

from evocatnet.lupa.lupa import lua_table_to_python_dict # may need to add PYTHONPATH in main program so this is found

#------------------------------------------------------------------------

# return a set of savedVar names
def get_savedVarNames(lua_code):
    #print('get_savedVarNames',file=sys.stderr)
    savedVarNames = set()
    lines = lua_code.split('\n')
    for line in lines:
        if m:=re.match(r'([a-zA-Z_][a-zA-Z0-9_]*)\s*=',line):
            savedVarNames.add(m.group(1))

    return savedVarNames

#------------------------------------------------------------------------

# Read in a saved variables file up to the account data corresponding to the
# given savedVarFile,savedVarName,server,account_name.

# No need to specify <savedVarName> if only a single variable was saved.
# No need to specify <server> if saves only exist for a single server. <server> is one of 'EU', 'NA', 'PTS'
# No need to speify <account_name> if saves only exist for a single account_name for the given server.

# Any value of opts is passed to lua_table_to_python_dict() function
# (typically ops={'keep_as_dict': True} to not try to convert lua tables that look like lists into python lists).

def readAccountData(savedVarFile,savedVarName=None,server=None,account_name=None,opts={},debug=0):
    lua = LuaRuntime(unpack_returned_tuples=True)

    # Remove any non-_G globals (with given exceptions) so lua can't do file io etc
    for key in list(lua.globals()):
        if key != '_G' and key != 'ipairs':
            del lua.globals()[key]

    with open(savedVarFile,'r') as f:
        lua_code = f.read()

    savedVarNames = get_savedVarNames(lua_code)
    if savedVarName is not None:
        if savedVarName not in savedVarNames:
            raise RuntimeError(f"savedVarName {savedVarName} specified, but is not in SavedVariables file")
    else:
        if len(savedVarNames) == 0:
            raise RuntimeError(f"no savedVarNames could be found in SavedVariables file")
        if len(savedVarNames) > 1:
            raise RuntimeError(f"more than 1 savedVarName found in SavedVariables file, but no savedVarName was specified")
        savedVarName = list(savedVarNames)[0]
    if debug:
        print(f"{savedVarName = }",file=sys.stderr)

    lua.execute(lua_code)

    get_python_lookup_table = lua.eval(f"function(lua_table_to_python) return lua_table_to_python({savedVarName}) end")
    varDump = get_python_lookup_table(lambda t: lua_table_to_python_dict(t,opts))
    if debug:
        print('varDump:',file=sys.stderr)
        pprint.pprint(varDump,sort_dicts=False,stream=sys.stderr)

    servers = list(varDump.keys())
    if server is not None:
        if server != 'PTS':
            server += ' Megaserver'
        if server not in servers:
            raise RuntimeError(f"server {server} specified, but is not in SavedVariables file for savedVarName {savedVarName}")
    else:
        if len(servers) == 0:
            raise RuntimeError(f"no servers (ie top level keys) in SavedVariables file for savedVarName {savedVarName}")
        if len(servers) > 1:
            raise RuntimeError(f"need to specify server (more than 1 server in SavedVariables file for savedVarName {savedVarName}")
        server = servers[0]
    if debug:
        print(f"{server = }",file=sys.stderr)

    server_data = varDump[server]

    account_names = list(server_data.keys())
    if account_name is not None:
        if account_name not in account_names:
            raise RuntimeError(f"account {account_name} specified, but is not in SavedVariables file for savedVarName {savedVarName}, server {server}")
    else:
        if len(account_names) == 0:
            raise RuntimeError(f"no accounts in SavedVariables file for savedVarName {savedVarName}, server {server}")
        if len(account_names) > 1:
            raise RuntimeError(f"need to specify --account (more than 1 account names in SavedVariables file for savedVarName {savedVarName}, server {server}")
        account_name = account_names[0]
    if debug:
        print(f"{account_name = }",file=sys.stderr)

    return server_data[account_name]

#------------------------------------------------------------------------

# Read in a per-character SavedVariables file <savedVarFile> as saved in an addon by something like:
# ZO_SavedVars:NewCharacterIdSettings(savedVarName,1,nil,{...},GetWorldName())
# (where eg savedVarName = 'bike9876BagsDumpVar')

# Refer to readAccountData() for more info on savedVarName,server,account_name,opts args.
#
# Returns the data corresponding to the given savedVarName/server/account_name/character.

def readCharacterSave(savedVarFile,character_name,savedVarName=None,server=None,account_name=None,opts={},debug=0):

    ac_data = readAccountData(savedVarFile,savedVarName,server,account_name,opts,debug)

    for charId,charData in ac_data.items():
        if charData['$LastCharacterName'] != character_name:
            continue
        return charData

    raise RuntimeError(f"character name {character_name} not found for savedVarName {savedVarName}, server {server}, account name {account_name}")

#------------------------------------------------------------------------

# Read in an account-wide SavedVariables file <savedVarFile> as saved in an addon by something like:
# ZO_SavedVars:NewAccountWide(SavedVarName,1,nil,{...},GetWorldName())
# (where eg savedVarName = 'bike9876CharacterDumpVar'

# Refer to readAccountData() for more info on savedVarName,server,account_name,opts args.
#
# Returns the data corresponding to the given savedVarName/server/account_name.

def readAccountSave(savedVarFile,savedVarName=None,server=None,account_name=None,opts={},debug=0):

    ac_data = readAccountData(savedVarFile,savedVarName,server,account_name,opts,debug)

    if len(ac_data.keys()) != 1 or list(ac_data.keys())[0] != '$AccountWide':
        raise RuntimeError(f"savedVarName {savedVarName}, server {server}, account name {account_name} must have a single element '$AccountWide'")

    return ac_data['$AccountWide']
