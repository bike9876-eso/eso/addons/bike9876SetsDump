# make_sets_table
# bike9876@エボ猫.コム
# Jan 2023
# See LICENSE
#
# Outputs (to stdout) a table showing the known non-craftable sets for the given ESO character, using the lua dump file in <lua_dump_file>
#
# Also outputs a table listing the number of pieces obtained from the 3 Pledge Masters (Maj al-Ragath, Glirion the Redbeard, Urgarlag Chief-bane (or non-English equivalents))
#
# Requirements: python packages tabulate, lupa, wcwidth (install with pip3 install <package> or (debian/ubuntu) sudo apt install python3-<package>)
# (wcwidth is only needed if the language was set to japanese or chinese in ESO)
#
# Usage:
# 
# Copy SavedVariables/bike9876SetsDump.lua to this directory
# (eg
#   cp ~/.steam/steam/steamapps/compatdata/306130/pfx/drive_c/users/steamuser/My\ Documents/Elder\ Scrolls\ Online/live/SavedVariables/bike9876SetsDump.lua .
# )
#
# Run
#   python3 make_set_table.py [--server <server>]
#
# eg
#   python3 make_set_table.py
# (if only one server, eg the EU server, is used).
#
#   python3 make_set_table.py --server EU
# or
#   python3 make_set_table.py --server NA
# if setsDump has been used for more than 1 server - then need to specify which server data to use.
#
# If there is more than 1 account for this server, need to specify --account (eg --account=@bike9876)
#
# The setsDump file is assumed to be "bike9876SetsDump.lua" in the current directory.
# Specify
# --dumpfile <dumpfile>
# to use a different file.
#
# The column giving the Set name has a max col width by default of 15 chars.
# Specify
# --setname-maxcolwidth <maxcolwdith>
# to change this.
#
# The data for each set is presented as a table with by default a max of 9 columns.
# Specify
# --ncols <ncols>
# to change this.

# SavedVariables/bike9876SetsDump.lua has the form:
'''
bike9876SetsDumpVar =
{
    ["EU Megaserver"] =
    {
        ["@<accountname>"] =
        {
            ["$AccountWide"] =
            {
                ["version"] = 1,
                ["clientLang"] = "en", -- can be "de", "en", "fr" etc
                ["i18n"] =
                {
                    ["SI_MASTER_WRIT_DESCRIPTION_SET"] = "Set",
                },
                ["sets"] =
                {
                    [513] = -- set id
                    {
                        ["name"] = "Talfyg's Treachery", -- set name
                        ["setType"] = 6,
                        ["setTypeName"] = "Dungeon",
                        ["dlcName"] = "Stonethorn",
                        ["dlcTypeName"] = "DLC_TYPE_DUNGEONS",
                        ["numPieces"] = 22,
                        ["catId"] = 87, -- identifies where the set is found
                        ["zoneIds"] = -- zone ids obtained from both the set id and the cat id.
                        {
                            ["fromSetId"] = { 1160, 1201, 1160 },
                            ["fromCatId"] = { 1201 },
                        },
                        -- ["undauntedChestId"] = 1, -- if this has a Shoulder piece obtainable from a Pledge Master, the pledge master id is here
                        -- ["undauntedName"] = "Maj al-Ragath", -- as above, but the (localized) name of the Pledge Master
                        ["armor"] =
                        {
                            [164308] = -- piece id
                            {
                                [1] = 3, -- equipment type
                                [2] = "Chest", -- equipment type name
                                [3] = 1, -- armor type
                                [4] = "Light", -- armor type name
                                [5] = false, -- is not unlocked
                            },
                            ... (more armor)
                        },
                        ["weapons"] =
                        {
                            [164304] = -- piece id
                            {
                                [1] = 6, -- equipment type
                                [2] = "Two-Handed", -- equipment type name
                                [3] = 13, -- weapon type
                                [4] = "Frost Staff", -- weapon type name
                                [5] = false, -- is not unlocked
                            },
                            ... (more weapons)
                        }
                    },
                    ... (more sets)
                },
                ["zoneNames"] =
                {
                    [1160] = "Western Skyrim",
                    [1201] = "Castle Thorn",
                    ...
                },
                ["constants"] =
                {
                    ["dropMechanic"] =
                    {
                        [1] = "LIBSETS_DROP_MECHANIC_MAIL_PVP_REWARDS_FOR_THE_WORTHY",
                        ...
                    },
                    ["setType"] =
                    {
                        [1] = "LIBSETS_SETTYPE_ITERATION_BEGIN",
                        ...
                    }
                }
            }
        }
    }
}        
(the strings output are language-localised - the above assumes the user had English as their language when they dumped the sets from ESO)
'''

import sys
import re
import math
import argparse
import pprint
from tabulate import tabulate
from lupa import LuaRuntime
from operator import itemgetter
import textwrap

from lib.saved_variables import readAccountSave

# Indices into the lists of armor or weapon pieces
EQUIPTYPE = 0
EQUIPTYPENAME = 1
ARMORTYPE = 2
ARMORTYPENAME = 3
WEAPONTYPE = 2
WEAPONTYPENAME = 3
ISUNLOCKED = 4

# Special values of armor type
ARMORTYPE_NONE = 0

debug = 0

#----------------------------------------

# Create setGroups, each element is all the sets that are in a given region (ie (setType,zoneName)) (eg 'Overland', 'Auridon')
# ie setGroups has the form
'''
[{'introtext': ['Overland', 'Auridon'],
  'sets': [{'name': "Queen's Elegance",
            'pieces': {'armor': {(1,4): {'isUnlocked': True,'name': ('Shoulders', 'Light')}, # (4,1) is (<armortype,equipmenttype>)
                                 (1,8): {'isUnlocked': True, 'name': ('Waist', 'Light')},
                                 ... (more armor pieces)
                                 },
                       'weapons': {(6,5): {'isUnlocked': False, 'name': ('Two-Handed', 'Axe')}, # (6,5) is (<equipmenttype,weapontype>)
                                   (6,6): {'isUnlocked': False, 'name': ('Two-Handed', 'Mace')},
                                   ... (more weapon pieces)
                                  }
                      }
          },
          {'name': 'Twin Sisters',
            'pieces': {'armor': {
                                ...
                                },
                       'weapons': {
                                ...
                                }
                      }
          },
          ... (more sets in this group)
         ]
 },
 ... more groups
 ]

(armor uses keys (<armortype,equipmenttype>) whereas weapons use keys (<equipmenttype,weapontype>) because I want them to be sorted
differently on output)
'''

def get_setGroups(sets):

    setGroup = None
    setGroups = []
    introtext_current = None

    for set in sets:
        introtext = set['introtext']

        if introtext != introtext_current:
            if setGroup is not None:
                setGroups.append(setGroup)
            setGroup = {'introtext': introtext, 'sets': []}
            introtext_current = introtext

        setGroup_set = {'name': set['name'], 'pieces': {'armor':{}, 'weapons':{}}}
        for pieceId,piece in set['armor'].items():
            eq_arm = (piece[ARMORTYPE],piece[EQUIPTYPE]) # eg (1,1) for (in en) ("Light", "Head") -- this defines the order the armor will be output (all no-weight first (ie jewelry), then "Light" etc)

            if piece[ARMORTYPE] == ARMORTYPE_NONE:
                armorName = (piece[EQUIPTYPENAME],)
            else:
                armorName = (piece[EQUIPTYPENAME],piece[ARMORTYPENAME])
            setGroup_set['pieces']['armor'][eq_arm] = {'isUnlocked': piece[ISUNLOCKED], 'name': armorName}

        for pieceId,piece in set['weapons'].items():
            eq_weap = (piece[EQUIPTYPE],piece[WEAPONTYPE]) # eg (5,2) for (in en) ("One-Handed","Mace") -- this defines the order the weapons will be output (all One-Handed first, then Two-Handed, then Off Hand)

            setGroup_set['pieces']['weapons'][eq_weap] = {'isUnlocked': piece[ISUNLOCKED], 'name': (piece[EQUIPTYPENAME],piece[WEAPONTYPENAME])}

        setGroup['sets'].append(setGroup_set)

    #pprint.pp(setGroups)

    return setGroups

#----------------------------------------

#
# Given rows = [headers,sep,row] (a header line, a separating line, and the row data)
# Split it into multiple headers/sep/rows if row has more than args.ncols elements.
# (headers[0], sep[0], row[0] are kept blank in subsequent rows as these correspond to the Set Name
# which doesn't need repeating).

def row_split(rows,args):
    headers = rows[0]
    sep = rows[1]
    row = rows[2]

    w = args.ncols
    n = math.ceil(len(row)/w)

    #print(f'{w = }, {len(row) = }, {n = }')

    if n < 2:
        return rows

    rows_split = []
    for j in range(n):
        if j == 0:
            headers_split = headers[0:w]
            sep_split = sep[0:w]
            row_split = row[0:w]
            rows_split.extend([headers_split,sep_split,row_split])
        else:
            headers_split = headers[(w-1)*j+1:(w-1)*j+w]
            headers_split.insert(0,' ')
            sep_split = sep[(w-1)*j+1:(w-1)*j+w]
            sep_split.insert(0,' ')
            row_split = row[(w-1)*j+1:(w-1)*j+w]
            row_split.insert(0,' ')
            rows_split.extend([sep_split,headers_split,sep_split,row_split])

    return rows_split

#----------------------------------------

# get_table(set,ii18n,args)

# Get a table (header and row(s)) for the given set <set>.
# The data for a set will be split into multiple rows if it needs more than args.ncols columns.
# The header is treated as a row, as if the row is split, the tabulate module cannot handle this.
# i18n provides i18n strings.

def get_table(set,i18n,args):

    #print('set=')
    #pprint.pp(set)

    set_str = i18n['SI_MASTER_WRIT_DESCRIPTION_SET'] # 'Set' (with possible following non-words chars) in user's eso language
    m = re.match(r'\w+',set_str)
    if m is None:
        set_str = 'Set'
    else:
        set_str = m.group(0)

    armor = set['pieces']['armor']
    weapons = set['pieces']['weapons']

    #print('armor=')
    #pprint.pp(armor)

    #print('weapons=')
    #pprint.pp(weapons)

    eq_arms_sorted = sorted(armor.keys()) # eg [(1,1),(2,0),(3,1),...]
    eq_weaps_sorted = sorted(weapons.keys()) # eg [(5,1),(5,2),...]

    #print('eq_arms_sorted=')
    #pprint.pp(eq_arms_sorted)

    #print('eq_weaps_sorted=')
    #pprint.pp(eq_weaps_sorted)

    headers = ['\n'.join(armor[eq_arm]['name']) for eq_arm in eq_arms_sorted] # eg ['Head\nLight','Neck','Chest\nLight',...]

    headers.extend(['\n'.join(weapons[eq_weap]['name']) for eq_weap in eq_weaps_sorted]) # eg ['One-Handed\nAxe',...]
    headers.insert(0,set_str)
    #print('headers=')
    #pprint.pp(headers)

    row = [textwrap.fill(set['name'],args.setname_maxcolwidth)] # wrap the set name if its length > args.setname_maxcolwidth
    nunlocked = 0
    nset = len(armor)+len(weapons)
    for eq_arm in eq_arms_sorted:
        if armor[eq_arm]['isUnlocked']:
            row.append('+')
            nunlocked += 1
        else:
            row.append('')

    for eq_weap in eq_weaps_sorted:
        if weapons[eq_weap]['isUnlocked']:
            row.append('+')
            nunlocked += 1
        else:
            row.append('')

    headers.append(f'n/{nset}')
    row.append(nunlocked)

    separator = ['-----'] * len(row)

    #print('headers before splitting = ',headers)
    #print('row before splitting = ',row)

    # Split the row into multiple rows if needed
    rows = row_split([headers,separator,row],args)

    #print('rows after splitting = ')
    #print(rows)

    return rows

#----------------------------------------

# get_tables(sets,i18n,args)

# Given the sets (and i18n strings), return a list of tables of the form:
'''
[
 {'introtext': <introtext>, # a list of regions, eg ['Overland', 'Auridon']
  'tables': # a table for each set in this region
    [{'headers': <table headers for set>,
      'rows': <table rows for set>
     }
     ,
     ... repeat for other sets in this region
    ]
 },
 ... repeat for other regions

'''

def get_tables(sets,i18n,args):

    tables = []

    # Get the sets grouped according to where they are found
    setGroups = get_setGroups(sets)
    #pprint.pp(setGroups)

    for setGroup in setGroups:
        region = {'introtext': setGroup['introtext'], 'tables': []}
        for set in setGroup['sets']:
            rows = get_table(set,i18n,args)
            region['tables'].append(rows)

        tables.append(region)

    return tables

#----------------------------------------

# add_introtext(sets,zoneNames,constants,args)
# Add <introtext> (a list of values) to the set data, eg ['Overland','Auridon']

def add_introtext(sets,zoneNames,constants,args):

    for esoset in sets:
        esosetType = constants['setType'][esoset['setType']-1] # eg 'LIBSETS_SETTYPE_MONSTER'

        introtext = [esoset['setTypeName']]

        zoneIds = {}
        zoneIds['fromCatId'] = esoset['zoneIds']['fromCatId'] if 'fromCatId' in esoset['zoneIds'] else []
        zoneIds['fromSetId'] = esoset['zoneIds']['fromSetId'] if 'fromSetId' in esoset['zoneIds'] else []

        zones = {}
        for zoneNameSrc in ('fromCatId','fromSetId'):
            zones[zoneNameSrc] = [zoneNames[zoneId] for zoneId in zoneIds[zoneNameSrc]]

        # For monster sets, just use the first 2 fromSetId zones
        if esosetType == 'LIBSETS_SETTYPE_MONSTER' and zones['fromSetId']:
            introtext.extend(zones['fromSetId'][0:2])
        else:
            setZoneNames = {}
            for zoneNameSrc in ('fromCatId','fromSetId'):
                if zones[zoneNameSrc]:
                    zone1 = zones[zoneNameSrc][0] # only use 1st zonename
                    setZoneNames[zone1] = True # put zone1 values into a dict so avoid repetition.

            introtext.extend(setZoneNames.keys())

        esoset['introtext'] = introtext

#----------------------------------------

# min_armor_weight(set)
#
# Return the minimum armor weight of the pieces of armor in a set (1=Light, 2=Medium, 3=Heavy, 0=no armor weights)

def min_armor_weight(set):
    if 'armor' not in set:
        return None

    min = None
    for pieceId,piece in set['armor'].items():
        wgt = piece[ARMORTYPE]
        if wgt != 0 and (min is None or wgt < min):
            min = wgt

    #print(f'set {set["name"]}: min weight {min}')

    if min is None:
        return 0
    else:
        return min

#----------------------------------------

# Output the tables of sets, grouped by region, but one (single-rowed) table per set.

# do_tabulate(tables,file)

def do_tabulate(tables,file=sys.stdout):

    for region in tables:
        introtext = ' / '.join(region['introtext'])
        print(introtext,file=file)
        print('='*len(introtext),file=file)

        for table in region['tables']:
          print(tabulate(table),file=file)
          print(file=file)

        print('\n',file=file)

#----------------------------------------

# Return the coffer name (eg "Darkshade Caverns", "Sands and Madness", "Ascending Tide")
# for a given set with undaunted pledge master id undauntedId.

# Only coffer names in English are currently supported.

def get_coffer_name(clientLang,undauntedId,set):

    if undauntedId == 1: # Glirion the Redbeard - coffer names follow dungeon name if they end in I or II, others have custom name
        coffer_name = set['introtext'][1]
        if re.search(r' I+$',coffer_name):
            coffer_name = re.sub(r'^The ','',coffer_name)
            coffer_name = re.sub(r' I+$','',coffer_name)
        else:
            zoneId = set['zoneIds']['fromSetId'][0]
            if zoneId == 64 or zoneId == 449:
                coffer_name = 'Frigid Crucible'
            elif zoneId == 22 or zoneId == 11:
                coffer_name = 'Sands and Madness'
            elif zoneId == 148 or zoneId == 38:
                coffer_name = 'Serpents and Sailors'
            elif zoneId == 31 or zoneId == 131:
                coffer_name = 'Winds and Webs'
            else:
                raise RuntimeError(f"bad zoneId {zoneId} for {set=}")
    elif undauntedId == 2: # Maj al-Ragath - coffer name follows the dungeon name, eg "Banished Cells"
        coffer_name = set['introtext'][1]
        # Eg 'The Banished Cells I' -> 'Banished Cells'
        coffer_name = re.sub(r'^The ','',coffer_name)
        coffer_name = re.sub(r' I+$','',coffer_name)
    elif undauntedId == 3: # Urgarlag Chief-bane - coffer name follows DLC name
        dlcName = set['dlcName']
        if dlcName == '' or dlcName == 'Elder Scrolls Online':
            raise RuntimeError(f"bad dlcName '{dlcName}' for {undauntedId=}")
        coffer_name = dlcName
    else:
        raise RuntimeError(f"bad undauntedId {undauntedId}")

    return coffer_name

#----------------------------------------

# Output the number of items obtained from the coffers of each of the Undaunted pledge masters

def undaunted_coffers(clientLang,sets,file=sys.stdout):

    undaunted_sets = {}

    for set in sets:
        if 'undauntedChestId' in set and set['undauntedChestId']:
            undauntedId = set['undauntedChestId'] # eg 2
            undauntedName = set['undauntedName'] # eg (in English) "Glirion the Redbeard"

            if undauntedName not in undaunted_sets:
                undaunted_sets[undauntedName] = {'total': 0, 'known': 0, 'coffers': {}}

            coffer_name = get_coffer_name(clientLang,undauntedId,set)

            if coffer_name not in undaunted_sets[undauntedName]['coffers']:
                undaunted_sets[undauntedName]['coffers'][coffer_name] = {'known': 0}

            for pieceId,piece in set['armor'].items():
                undaunted_sets[undauntedName]['total'] += 1
                if piece[ISUNLOCKED]:
                    undaunted_sets[undauntedName]['known'] += 1
                    undaunted_sets[undauntedName]['coffers'][coffer_name]['known'] += 1

    for name,data in undaunted_sets.items():
        r = 5 # how many attempts can have from Mystery Coffer for price of an attempt from a normal Coffer
        T = undaunted_sets[name]['total']
        N = undaunted_sets[name]['known']
        M = T-N
        E5 = M*(1-(1-1/T)**r) # the expected number of new (unknown) set items from r goes
        undaunted_sets[name]['percent_known'] = float(f"{N/T*100: .1f}")
        undaunted_sets[name]['E5'] = float(f"{E5: .2f}")

        # Calculate the expected number of new (unknown) sets from a single purchase from a normal Coffer, for each Coffer
        for coffer_name,coffer_data in undaunted_sets[name]['coffers'].items():
            T = 12
            N = coffer_data['known']
            M = T-N
            E = M/T
            #print(f"{coffer_name=}, {T=}, {N=}, {M=}, {E=}")
            coffer_data['E'] = float(f"{E: .2f}")

    print("Shoulder pieces from Undaunted Pledge Master coffers (max 12 per coffer):",file=file)
    print("===============================================================",file=file)
    print("(See below for explanation of 'E' and 'E5'")
    print()
    pprint.pprint(undaunted_sets,width=120)
    print()
    print("E is the expected number of new set items when buying once from this Coffer (must always be <= 1).")
    print("E5 is the expected number of new set items when buying 5 Mystery Coffer items from this vendor (must always be <=5).")
    print("If E>E5 for any Coffer, then it is worth buying from that Coffer (if all you want is to maximise the chance of getting an unknown set item).")

#----------------------------------------

# Process the sets in lua table <setsDump>

def process_dump(args):

    accountDump = readAccountSave(args.dumpfile,server=args.server,account_name=args.account,opts={},debug=debug)
    #print('accountDump = ')
    #pprint.pprint(accountDump,sort_dicts=False)

    if 'sets' not in accountDump:
        raise RuntimeError(f"setsDump does not have 'sets' element (server={args.server}, account={args.account}")

    # get the i18n strings, the zone names, and the sets from the sets dump. Only keep the _values_ of the sets (ie discard the setId keys)
    (clientLang,i18n,zoneNames,constants,sets) = (accountDump[k] for k in ['clientLang','i18n','zoneNames','constants','sets'])
    sets = sets.values()

    zoneNames[0] = '-'

    # Discard any sets with numPieces = 0.
    # These (I think) correspond to sets that are no longer available in-game, eg Arms of Infernace.
    # If a player acquired these I'm hoping they will have numPieces>0, so won't be discarded.
    sets = [set for set in sets if set['numPieces']>0]
    #pprint.pp(sets)

    # Add <introtext> (a list of values) to the set data, eg ['Overland','Auridon']
    add_introtext(sets,zoneNames,constants,args)

    # Sort sets according to keys. The last key will be the most "major" key.
    sets = sorted(sets,key=lambda set: min_armor_weight(set))
    sets = sorted(sets,key=itemgetter('introtext'))
    sets = sorted(sets,key=itemgetter('catId'))
    #pprint.pp(sets)

    tables = get_tables(sets,i18n,args)

    do_tabulate(tables)

    undaunted_coffers(clientLang,sets)

#----------------------------------------

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--server',help='server',choices=['EU','NA','PTS'],default=None)
    parser.add_argument('--account',help='account',default=None)
    parser.add_argument('--dumpfile',default='bike9876SetsDump.lua')
    parser.add_argument('--setname-maxcolwidth',type=int,default=15,help='max width of "Set name" column (default %(default)i)')
    parser.add_argument('--ncols',type=int,default=9,help='max number of columns in a table row (default %(default)i)')

    process_dump(parser.parse_args())

#----------------------------------------

main()
