# evocatnet/lib/lupa.py
# @bike9876 Jan 2023

import lupa

# Try to turn a dict into a list. Return either the original dict, or the listified version of it,
# eg {1: 'a', 2: 'b'} would be returned as ['a',b']
# If opts['recursive'] is true, then run this recursively (only done if the parent dict is successfully turned into a list)
def dict_to_list(d,opts={}):
    if type(d) != dict:
        return d
    k = 1
    while k in d:
        k += 1
    if k <= len(d):
        return d
    seq = list(dict(sorted(d.items())).values()) # sort d by key, then return values of sorted dict (try to map the values to lists if 'recursive' option set)
    if 'recursive' in opts and opts['recursive']:
        return [dict_to_list(v,opts) for v in seq]
    else:
        return seq

# Given a lua table t, return the python dict/list version of it
#
# eg
# t = lua.eval("{ ['a'] = 32, ['b'] = {b2=3, b3={3,4,{x='abc',z=false}}, b4={}}, }")
# d = lua_table_to_python_dict(t)
# expect d = {'a': 32, 'b': {'b4': {}, 'b3': [3, 4, {'z': False, 'x': 'abc'}], 'b2': 3}}
# (note the lua sequence {3,4,{x=...}} is detected as a list. But the empty table {} is detected as an empty dict.
#
# if opts['keep_as_dict'] is true, then do not attempt to identify (and turn into lists) lua sequences. 
#
def lua_table_to_python_dict(t,opts={}):

    if lupa.lua_type(t) != 'table':
        return t
    if len(list(t)) == 0:
        return {}

    d = {}
    for k,v in t.items():
        if lupa.lua_type(v) == 'table':
            v = lua_table_to_python_dict(v,opts)
        d[k] = v

    # (if opts['keep_as_dict'] is not true) check to see if sequence (there's no guarantee the keys of d are sorted)
    if 'keep_as_dict' in opts and opts['keep_as_dict']:
        return d

    return dict_to_list(d,{'recursive':True})
